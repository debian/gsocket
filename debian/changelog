gsocket (1.4.43-4) unstable; urgency=medium

  * Update symbols file for loong64. (Closes: #1074293)
    + Thanks to zhangdandan for the patch!

 -- Daniel Echeverri <epsilon@debian.org>  Thu, 27 Jun 2024 21:48:58 -0500

gsocket (1.4.43-3) unstable; urgency=medium

  * Update symbols file, ignore symbol on ppc64el

 -- Daniel Echeverri <epsilon@debian.org>  Sat, 08 Jun 2024 22:33:41 -0500

gsocket (1.4.43-2) unstable; urgency=medium

  * Fix FTBFS in some archs. (Closes: #1071928)

 -- Daniel Echeverri <epsilon@debian.org>  Sun, 02 Jun 2024 21:21:19 -0500

gsocket (1.4.43-1) unstable; urgency=medium

  * New upstream version 1.4.43 (Closes: #1065966)
  * debian/copyright
    + Extend debian copyright holders years.
  * debian/control
    + Bump Standards-Version to 4.7.0 (no changes).
  * Update debian/gsocket.symbols file.

 -- Daniel Echeverri <epsilon@debian.org>  Fri, 24 May 2024 17:54:48 -0500

gsocket (1.4.41-1) unstable; urgency=medium

  * New upstream version 1.4.41

 -- Daniel Echeverri <epsilon@debian.org>  Sat, 30 Sep 2023 15:15:17 -0500

gsocket (1.4.40-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    + Bump Standards-Version to 4.6.2 (no changes).
    + Add dpkg-dev package in Depends.
  * debian/copyright
    + Extend debian copyright holders years.

 -- Daniel Echeverri <epsilon@debian.org>  Sun, 18 Jun 2023 14:25:04 -0500

gsocket (1.4.39-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/watch file.
  * Update lintian-overrides.
  * Update debian/gsocket.symbols file.

 -- Daniel Echeverri <epsilon@debian.org>  Thu, 22 Dec 2022 09:48:41 -0500

gsocket (1.4.37-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    + Bump Standards-Version to 4.6.1 (no changes).
  * debian/copyright
    + Extend debian copyright holders years.

 -- Daniel Echeverri <epsilon@debian.org>  Fri, 08 Jul 2022 20:26:19 -0500

gsocket (1.4.33-2) unstable; urgency=medium

  * Mark test 6.1 as flaky test. (Closes: #1005890)
  * Fix typo in d/upstream/metadata file.

 -- Daniel Echeverri <epsilon@debian.org>  Sat, 19 Feb 2022 11:18:52 -0500

gsocket (1.4.33-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    + Bump Standards-Version to 4.6.0 (no changes)
  * debian/patches
    + Remove all patches.
      + Merge with upstream.

 -- Daniel Echeverri <epsilon@debian.org>  Fri, 27 Aug 2021 22:28:02 -0500

gsocket (1.4.29-1) unstable; urgency=medium

  * Initial release (Closes: #986297)

 -- Daniel Echeverri <epsilon@debian.org>  Fri, 09 Apr 2021 23:51:38 -0500
